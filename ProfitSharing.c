/**************************************************************************
	Profit Sharning
 **************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>

// 状態
#define START 0 // 入口 (スタート)
#define WOOD  1 // 森
#define LAKE  2 // 湖
#define POND  3 // 池
#define FIELD 4 // 草原
#define GOAL  5 // 宝 (ゴール)

// 行動
#define EAST  0 // 東
#define WEST  1 // 西
#define SOUTH 2 // 南
#define NORTH 4 // 北

#define TrialNo 100  // 試行回数
#define StateNum 6  // 状態数
#define ActNum 4    // 行動数

#define TimeMax 100 // エピソードの長さの上限

#define Cbid 0.1    // 係数(競り値)
#define Reward 10   // 報酬

/*********************************************************************
	乱数のシードの生成
 **********************************************************************/
void init_rnd()
{
	srand((unsigned int)time(NULL));
}

/********************************************************************
	乱数の発生 (0〜1の乱数)
 ********************************************************************/
double Random()
{
	return((double)rand()/RAND_MAX);
}

/********************************************************************
	ルールの価値の初期化
	Qvalue[s][a] : 状態sにおいて行動aを取ることの価値
 ********************************************************************/
void init_Q(double Qvalue[StateNum][ActNum])
{
	/* すべての状態と行動の価値を0にする */
	for(int s = 0; s < StateNum; s++){
		for(int a = 0; a < ActNum; a++){
			Qvalue[s][a] = 0.00;
		}
	}
}

/********************************************************************
	状態遷移リストと行動リストの初期化
	state_list[t] : 時刻tにおける状態
	act_list[t]   : 時刻tにおける行動
 ********************************************************************/
void init_list(int state_list[TimeMax], int act_list[TimeMax])
{
	int t;

	/* すべての時刻(t=0～TimeMax-1)の
		 状態遷移リストと行動リストの値を-1にする */
	for(t = 0; t < TimeMax; t++){
		state_list[t] = -1;
		act_list[t] = -1;
	}

}

/********************************************************************
	行動の選択 (ボルツマン選択)
state        : 状態
Qvalue[s][a] : 状態sにおいて行動aを取ることの価値
env[s][a]    : 状態sにおいて行動aを取ったときに遷移する状態
t            : 試行回数
 ********************************************************************/
int select_action(int state,
		double Qvalue[StateNum][ActNum],
		int env[StateNum-1][ActNum],int t)
{
	double sum = 0;    // ルールの価値の合計
	int a;         // 行動
	double r;      // 乱数 (0～sumの間の乱数)
	double border; // 境界線
	double T=10;   // 温度パラメータ

	/* 温度パラメータを設定 */
	T=T-t;
	if(T<=1) T=0.1;

	/* ルールの価値の合計を計算
		 その状態で取れない行動(env[state][a]=-1)
		 の価値は合計には含まない */
	for(int a = 0; a < ActNum; a++){
		if(env[state][a] != -1){
			sum += Qvalue[state][a];
		}
	}

	/* 0～sumの乱数を生成 */
	r = Random()*sum;
	border=0;
	for(a=0;a<ActNum;a++){
		/* 取ることのできる行動の中から行動を選択 */
		if(env[state][a]!=-1){
			border += exp(Qvalue[state][a]/T);
		}
		/* 選択された行動を返す */
		if(r<=border){
			return a;
		}
	}
}

/********************************************************************
	強化関数
w : エピソードの長さ
t : 時刻
 ********************************************************************/
double F_func(int w, int t)
{

	/* 1/((取り得る行動数+1)の(w-1-t)乗)を返す */
	return 1 / pow((ActNum + 1),w-1-t);

}

/********************************************************************
	ルールの価値の更新 (逐一訪問法)
	Qvalue[s][a]  : 状態sにおいて行動aを取ることの価値
	state_list[t] : 時刻tにおける状態
	act_list[t]   : 時刻tにおける行動
w             : エピソードの長さ
r             : 報酬
 ********************************************************************/
void update_Q(double Qvalue[StateNum][ActNum],
		int state_list[TimeMax],
		int act_list[TimeMax], int w, int r)
{
	int t;

	/* すべての時刻(0～t-1)についてルールの価値を更新 */
	/*** この部分を自分で書く ***/

}

/**************************************************************************
	メインプログラム
 **************************************************************************/
int main()
{
	int t;
	int s,a;
	int act;     // 行動
	int state;   // 状態
	double Qvalue[StateNum][ActNum]; //ルールの価値
	// 環境 (状態の遷移) 東  西  南  北
	int env[StateNum-1][ActNum]={{WOOD, -1,    POND, -1},    // 入口
		{LAKE, START, FIELD,-1},    // 森
		{-1,   WOOD,  GOAL, -1},    // 湖
		{FIELD,-1,    -1,   START}, // 池
		{-1    ,POND, -1,   WOOD}}; // 草原

	int act_list[TimeMax];   // 行動のリスト
	int state_list[TimeMax]; // 状態のリスト
	int count; // エピソードの長さ
	char *states[StateNum]={"入口","森","湖","池","草原","宝"}; // 状態(表示用)
	char *acts[ActNum]={"東","西","南","北"};  // 行動(表示用)
	FILE *fp; // ファイルポインタ

	/* 結果保存用のファイル(result.dat)をオープン */
	if((fp=fopen("result.dat","w"))==NULL){
		printf("main() : Cannot open \"result.dat\"\n");
		exit(1);
	}

	init_rnd();      /* 乱数の初期化 */
	init_Q(Qvalue);  /* ルールの価値の初期化 */

	/* TrialNo回の試行を繰り返す */
	for(t=0;t<TrialNo;t++){
		printf("[%d]",t);
		init_list(state_list,act_list); /* 状態リスト、行動リストの初期化 */
		state = START; /* 状態を初期化(STARTに設定) */
		count=0; /* エピソードの長さを0で初期化 */
		/* ゴールに到達するかエピソードの長さの上限に到達するまで繰り返す */
		while(state!=GOAL&&count<TimeMax){
			state_list[count]=state; /* 状態リストに状態を保存  */
			act=select_action(state,Qvalue,env,t); /* 行動を決定 */
			printf("%s==>(%s)==>",states[state],acts[act]); /* 状態と行動を表示 */
			act_list[count]=act; /* 行動リストに行動を保存  */
			state=env[state][act]; /* 行動することによって状態が遷移 */
			count++; /* エピソードの長さを1増やす */
		}
		/* ルールの価値を更新 */
		update_Q(Qvalue,state_list,act_list,count,Reward);
		printf("%s\n",states[state]); /* 最終的な状態を表示 */
		fprintf(fp,"%d %d\n",t,count); /* 試行回数とエピソード長をファイルに出力 */
	}
	fclose(fp); /* ファイルをクローズ */

	/* 最終的なルールの価値保存用のファイルをオープン */
	if((fp=fopen("Q.dat","w"))==NULL){
		printf("main() : Cannot open \"Q.dat\"\n");
		exit(1);
	}


	/* ルールの価値をファイルに書き出す */
	fprintf(fp," 東 西南北\n");
	for(s=0;s<StateNum;s++){
		fprintf(fp,"%s\t",states[s]);
		for(a=0;a<ActNum;a++){
			fprintf(fp,"%6.3lf\t",Qvalue[s][a]);
		}
		fprintf(fp,"\n");
	}

	/* ファイルをクローズ  */
	fclose(fp);

	return 0;
}

