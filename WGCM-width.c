/**************************************************************************
	狼と山羊とキャベツと男 (幅優先探索)
 **************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>

#define MAN     0
#define WOLF    1
#define GOAT    2
#define CABBAGE 3

#define TimeMax 50

//自分で定義した定数
const int member = 4;	//メンバーの数

int state[TimeMax][5]; /* 男がいる側の状態
													state[t][4]が 0なら左岸 1なら右岸 */
int prev_state[TimeMax]; /* 過去の状態の履歴 */

/**************************************************************************
	状態の表示
	引数 state[i] : 左岸もしくは右岸の状態
	state[i]の配列の内容に応じて状態を表示する
	(例) state[i]={1,1,1,1}ならば [ 男 狼 山羊 キャベツ ]
	state[i]={1,0,1,0}ならば [ 男 山羊 ]
	state[i]={0,0,0,0}ならば [  ]
 **************************************************************************/
void print_state(int state[member]){
	char string[20] = "\0";	//プリントする変数を用意
	printf("[ ");	//カッコは一回しか必要ないので直だし
	/* 以下書く要素を巡回し、値がtrueであったときにはその名前を、
	 *																そうでなければnull文字を変数にセット */
	for(int i = 0; i < member; i++){
		switch(i){
			case MAN:
				if(state[i]){
					strcpy(string,"男");
				}else{
					strcpy(string,"\0");
				}
				break;
			case WOLF:
				if(state[i]){
					strcpy(string,"狼");
				}else{
					strcpy(string,"\0");
				}
				break;
			case GOAT:
				if(state[i]){
					strcpy(string,"山羊");
				}else{
					strcpy(string,"\0");
				}
				break;
			case CABBAGE:
				if(state[i]){
					strcpy(string,"キャベツ");
				}else{
					strcpy(string,"\0");
				}
				break;
			default:
				strcpy(string,"\0");
				break;
		}
		/* セットしたものを出力して次のループへ */
		printf("%s ",string);
	}
	printf("]");	//最後にカッコを閉じる
}


/**************************************************************************
	結果の表示
	引数 t : ステップ数
	tステップ目の結果を表示する
	ステップ数: [ 左岸の状態 ] [ 右岸の状態 ]
	(例) 0: [ 男 狼 山羊 キャベツ ] [ ]
1: [ 狼 山羊 ] [ 男 キャベツ ]
 **************************************************************************/
void print_ans(int t){
	int right[member];
	int left[member];

	/* prev_state[t]に値が入っていれば状態を表示 */
	if(prev_state[t]!=-1){
		print_ans(prev_state[t]);
	}

	/* 男が右岸にいる場合
		 right[] に state[t][] をコピー & left[]のデータを生成 */
	/* 男が左岸にいる場合
		 left[] に state[t][] をコピー & right[]のデータを生成 */
	if(state[t][4] == 1){	//男at右側
		for(int i = 0; i < member; i++){
			right[i] 	= state[t][i];
			left[i] 	= !(state[t][i]);
		}
	}else{	//そうでなければ男at左側
		for(int i = 0; i < member; i++){
			left[i] 	= state[t][i];
			right[i] 	= !(state[t][i]);
		}
	}
	/* tステップの状態を表示する
		 [ヒント] print_state()の関数が正しく作成されていれば
		 print_state(left_side[t]);  で tステップ目の左岸の状態、
		 print_state(right_side[t]); で tステップ目の右岸の状態
		 が表示できる */
	printf("%2d : ",t);
	print_state(left);
	print_state(right);
	printf("\n");

}

/**************************************************************************
	状態のチェック
	引数 T : ステップ数
	・狼と山羊、山羊とキャベツを残した状態でもなく、既に探索された状態
	でもなければ 1を返す
	・それ以外は 0を返す
 **************************************************************************/
bool check_state(int T){

	int i,t;
	int now_state[member];   /* 男がいない側の現在の状態 */
	bool samepast = true;

	/* 男がいない側の現在の状態をstate[T][]から生成し、now_state[]に代入 */
	for(int i = 0; i < member; i++){
		now_state[i] = !(state[T][i]);
	}

	/* 狼と山羊 もしくは 山羊とキャベツが一緒にないかをチェック
		 あれば0を返す */
	if(
			(now_state[WOLF] && now_state[GOAT]) ||
			(now_state[GOAT] && now_state[CABBAGE])
			){
		return false;
	}

	/* 過去に同じ状態がないかをチェック  あれば0を返す
		 [ヒント] state[t][i](tステップ目の状態)と
		 state[T][i](現在の状態)を比較してチェック */
	for(t=0;t<T;t++){
		for(int i = 0; i < sizeof(state[0])/sizeof(int); i++){
			//書く状態のオブジェクトの有無を乗算する
			samepast *= state[t][i] == state[T][i];
		}
		//すべて同じ場合はtrueが帰るのでその場合はfalse(過去と同じ)を返す
		if(samepast){
			return false;
		}
		samepast = true;	//次のためにリセット
	}
	/* いずれにも該当しなければ1を返す */
	return true;

}

/**************************************************************************
	幅優先探索
 **************************************************************************/
void search(){
	int i,j;
	/*  r の位置にあるデータを取り出して、新しい状態を生成できたら、w の位置
			に書き込む */
	static int r=0;
	static int w=1;

	int now_state[sizeof(state[0])/sizeof(int)]; /* 男がいる側の現在の状態 */
	int new_state[sizeof(state[0])/sizeof(int)]; /* 次のステップの男がいる側の状態 */

	for(;r<=w;r++){
		//printf("%d ",w);
		/* now_state[]に stage[r][]をコピー*/
		for(i=0;i<5;i++){
			now_state[i]=state[r][i];
		}
		for(i=0;i<member;i++){/* 0: 男 1: 狼 2: 山羊 3: キャベツ を順に調べる */
			if(now_state[i]==1){/* 移動できるのであれば(男と同じ側にいれば) */
				for(j = 0; j < sizeof(state[0])/sizeof(int); j++){
					if((i == j) || (j == 0)){
						//男と対岸に移動するオブジェクトはそのまま(移動先の情報を記録するため)
						new_state[j] = now_state[j];
					}else{
						//移動しないオブジェクトはビット反転(移動先にはいないから)
						new_state[j] = !(now_state[j]);
					}
				}

				/* state[w][]としてnew_state[]を保存 */
				for(j=0;j<sizeof(state[0])/sizeof(int);j++){
					state[w][j]=new_state[j];
				}

				/* prev_state[w]としてrを保存 */
				prev_state[w]=r;
				/* iと男を移動(iが0の場合は男のみ移動)した後の状態が有効かどうかを
					 チェック */
				if(check_state(w)){
					/* 右岸にすべてが移動していれば 結果を表示して終了 */
					if(state[w][0] &&state[w][1] &&state[w][2] &&state[w][3] &&state[w][4]){
						print_ans(w);
					}
					w++;
					break;
				}
			}
		}
	}
}

/**************************************************************************
	メインプログラム
 **************************************************************************/
int main()
{
	int i,t;

	/* 初期化 */
	for(i=0;i<member;i++){
		state[0][i]=1; /* 最初はすべて左岸に(男と一緒に)いる */
	}
	state[0][4]=0; /* 男は左側にいるので0にセット */

	for(t=0;t<TimeMax;t++){
		prev_state[t]=-1;
	}

	/* 探索 */
	search();
	return 0;
}

