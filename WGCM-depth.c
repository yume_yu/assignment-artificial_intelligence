/**************************************************************************
	狼と山羊とキャベツと男 (深さ優先探索)
 **************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>

#define MAN     0
#define WOLF    1
#define GOAT    2
#define CABBAGE 3

#define SearchMax 20

//自分で定義した定数
const int member = 4;	//メンバーの数

int left_side[SearchMax][4];   /* 左岸の状態 */
int right_side[SearchMax][4];  /* 右岸の状態 */

/**************************************************************************
	状態の表示
	引数 state[i] : 左岸もしくは右岸の状態
	state[i]の配列の内容に応じて状態を表示する
	(例) state[i]={1,1,1,1}ならば [ 男 狼 山羊 キャベツ ]
	state[i]={1,0,1,0}ならば [ 男 山羊 ]
	state[i]={0,0,0,0}ならば [  ]
 **************************************************************************/
void print_state(int state[4])
{
	char string[20] = "\0";	//プリントする変数を用意
	printf("[ ");	//カッコは一回しか必要ないので直だし
	/* 以下書く要素を巡回し、値がtrueであったときにはその名前を、
																			そうでなければnull文字を変数にセット */
	for(int i = 0; i < member; i++){
		switch(i){
			case MAN:
				if(state[i]){
					strcpy(string,"男");
				}else{
					strcpy(string,"\0");
				}
				break;
			case WOLF:
				if(state[i]){
					strcpy(string,"狼");
				}else{
					strcpy(string,"\0");
				}
				break;
			case GOAT:
				if(state[i]){
					strcpy(string,"山羊");
				}else{
					strcpy(string,"\0");
				}
				break;
			case CABBAGE:
				if(state[i]){
					strcpy(string,"キャベツ");
				}else{
					strcpy(string,"\0");
				}
				break;
			default:
				strcpy(string,"\0");
				break;
		}
		/* セットしたものを出力して次のループへ */
		printf("%s ",string);
	}
	printf("]");	//最後にカッコを閉じる
}

/**************************************************************************
	結果の表示
	引数 T : ステップ数
	Tステップ目までの結果を表示する
	ステップ数: [ 左岸の状態 ] [ 右岸の状態 ]
	(例) 0: [ 男 狼 山羊 キャベツ ] [ ]
1: [ 狼 山羊 ] [ 男 キャベツ ]
 **************************************************************************/
void print_ans(int T)
{
	int t;

	/* 初期状態からTステップ目までの結果を表示する
		 [ヒント] print_state()の関数が正しく作成されていれば
		 print_state(left_side[t]);  で tステップ目の左岸の状態、
		 print_state(right_side[t]); で tステップ目の右岸の状態
		 が表示できる */

	for(t=0;t<=T;t++){
		printf("%2d : ",t);
		print_state(left_side[t]);
		print_state(right_side[t]);
		printf("\n");
	}
}

/**************************************************************************
	状態のチェック
	引数 T                 : ステップ数
	state[i]          : チェックしたい状態
	past_state[t][i]  : 過去の状態(tステップ目の状態)
	・狼と山羊、山羊とキャベツを残した状態でもなく、既に探索された状態
	でもなければ 1を返す
	・それ以外は 0を返す
 **************************************************************************/
bool check_state(int T,int state[4], int past_state[SearchMax][4]){
	bool samepast = true;

	/* 狼と山羊 もしくは 山羊とキャベツが一緒にないかをチェック
		 あれば0を返す */

	if((state[WOLF] && state[GOAT]) || (state[GOAT] && state[CABBAGE])){
		return false;
	}

	/* 過去に同じ状態がないかをチェック  あれば0を返す
		 [ヒント] past_state[t][i](tステップ目の状態)と
		 state[i](現在の状態)を比較してチェック */
	for(int t=0;t<T;t++){
		for(int i = 0; i < member; i++){
			//書く状態のオブジェクトの有無を乗算する
			samepast *= state[i] == past_state[T][i];
		}
		//すべて同じ場合はtrueが帰るのでその場合はfalse(過去と同じ)を返す
		if(samepast){
			return false;
		}
		samepast = true;	//次のためにリセット
	}
	/* いずれにも該当しなければ1を返す */
	return true;
}

/**************************************************************************
	深さ優先探索
	引数 T                 : ステップ数
	src_side[t][i]    : 男がいる側の状態
	dest_side[t][i]   : 男がいない側の状態
 **************************************************************************/
void search(int T, int src_side[SearchMax][4], int dest_side[SearchMax][4]){
	int i,j;

	int src_state[4];      /* 男がいる側の状態 */
	int dest_state[4];     /* 男がいない側の状態 */
	int new_src_state[4];  /* 男がいる側の次のステップの状態 */
	int new_dest_state[4]; /* 男がいない側の次のステップでの状態 */

	/* Tステップ目の状態をコピー */
	for(i=0;i<4;i++){
		src_state[i]=src_side[T][i];
		dest_state[i]=dest_side[T][i];
	}

	for(i=0;i<4;i++){ /* 0: 男 1: 狼 2: 山羊 3: キャベツ を順に調べる */
		if(src_state[i]==1){ /* 移動できるのであれば(男と同じ側にいるのであれば) */
			/* iと男を移動(iが0の場合は男のみ移動)した後の状態を
				 new_src_state[], new_dest_state[] に格納
				 [ヒント] 現在の状態 (src_state[], dest_state[])を
				 new_src_state[], new_dest_state[]にコピーし,
				 iと男が移動した場合に値がどのように変化するかを設定 */
			for(j = 0; j < member; j++){
				if((i == j) || (j == 0)){	//移動するオブジェクとはビット反転
					new_src_state[j] = src_state[j];
					new_dest_state[j] = dest_state[j];
				}else{
					new_src_state[j] = !(src_state[j]);
					new_dest_state[j] = !(dest_state[j]);
				}
			}

			/* iと男を移動(iが0の場合は男のみ移動)した後の状態が有効かどうかを
				 チェックし、有効であれば 岸の状態を更新し、次に進む */
			if(check_state(T,new_dest_state,dest_side)){
				/* 男が左岸にいる場合(Tが偶数の場合)
					 left_side[T+1][]に new_src_state[]をコピー
					 right_side[T+1][]に new_dest_state[]をコピー */
				/* 男が右岸にいる場合(Tが奇数の場合)
					 right_side[T+1][]に new_src_state[]をコピー
					 left_side[T+1][]に new_dest_state[]をコピー */
				if(T%2==0){
					for(int j = 0; j < member; j++){
						left_side[T+1][j] = new_dest_state[j];
						right_side[T+1][j] = new_src_state[j];
					}
				}else{
					for(int j = 0; j < member; j++){
						left_side[T+1][j] = new_src_state[j];
						right_side[T+1][j] = new_dest_state[j];
					}
				}
				/* 右岸にすべてが移動していれば 結果を表示して終了 */
				if(	right_side[T+1][MAN] &&
						right_side[T+1][GOAT] &&
						right_side[T+1][WOLF] &&
						right_side[T+1][CABBAGE] ){
					print_ans(T+1);
					exit(0);
				}
				/* そうでなければ再帰的に探索を続ける */
				else{
					for(int j = 0; j < member; j++){
						dest_side[T+1][j] = new_dest_state[j];
						src_side[T+1][j] = new_src_state[j];
					}
					search(T+1,dest_side,src_side);
				}
				}		}
		}
	}

	/**************************************************************************
		メインプログラム
	 **************************************************************************/
	int main()
	{
		int i,t;

		/* 配列の初期化 (-1を設定) */
		for(t=0;t<SearchMax;t++){
			for(i=0;i<4;i++){
				left_side[t][i]=-1;
				right_side[t][i]=-1;
			}
		}

		/* 初期状態の設定 */
		for(i=0;i<4;i++){
			left_side[0][i]=1;
			right_side[0][i]=0;
		}

		/* 探索 */
		search(0,left_side,right_side);

		return 0;
	}

